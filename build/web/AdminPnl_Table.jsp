<%-- 
    Document   : Admin
    Created on : 07.03.2016, 14:45:30
    Author     : Natavan
--%>

<%@page import="com.tababat.db.ImageDb"%>
<%@page import="com.tababat.db.PrescriptionDb"%>
<%@page import="com.tababat.db.DiseaseDb"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
       
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
         <title>Admin Panel</title>
         
    </head>
   
    <body>
    <style>
         #logo{
            margin-top: -15px;
        }
         a:hover{
            text-decoration: none;
        }
        .search-query{
            margin-right: 30px;
        }
        ul{
            list-style: none;
        }
        ul li{
            float: left;
            margin:10px 10px 0px;
        }
        .navbar-inverse{           
            margin-bottom:0;
            margin-top:22%;
        }
    </style>
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-right animated flip" data-toggle = "collapse" data-target=".main-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class = "navbar-brand"><img id = "logo" src = "log.png" width = "50" height = "50"/></a>
            <a class = "navbar-brand" href="main.jsp">Dr.Tababat</a>

            <div class="navbar-collapse nav navbar-nav top-menu btn-group pull-right">
                <ul>
                    <li>
                    <form class="navbar-search">                        
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                    </li>
                    <li><a href="#">
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    <span class="label label-info">3</span>
                    </a>
                    </li>
                    <li><a href="main.jsp"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li> 
                    <li><a href="AdminReg.jsp">
                    <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                    log out
                    </a>
                    </li>                
                   </ul> 
            </div>            
        </div>
    </div>
        
<div class="ch-container">
    <div class="row">
        
        <!-- left menu -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="collapse navbar-collapse nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Navigation</li>
                        <li><a class="ajax-link" href="AdminPnl.jsp"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
                        </li>
                        <li class="active"><a class="ajax-link" href="AdminPnl_Table.jsp"><i
                                    class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>                        
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                        </li>
                    </ul>                    
                </div>
            </div>
        </div>

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
        <div>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
         </ul>
        </div>

    <div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
         <h2><i class="glyphicon glyphicon-stethoscope"></i> Disease</h2>
        <div class="box-icon">
            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
                <% 
                 request.setAttribute("allDiseases", DiseaseDb.getAllDiseases());
                 request.setAttribute("allPrescriptions", PrescriptionDb.getAllPrescriptions());
                 request.setAttribute("allImages", ImageDb.getAllImages());
                %>
   
                <table class="table table-striped" id="disease_table">
    <thead>
    <tr>
        <th>Disease ID</th>
         <th>Body Part</th>
        <th>Disease Name</th>
      
         <th>Actions</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${allDiseases}" var="dis">
    <tr>
        <td class="center" id='disease_id'>${dis.disease_id}</td>
        <td class="center" id="body_part">${dis.body_part}</td>
        <td class="center" id='disease_name'>${dis.disease_name}</td>        
        <td class="center">           
              <a data-toggle="model" class="btn btn-info disease-edit" href="" data-id="${dis.disease_id}" data-id2="${dis.body_part}" data-id3="${dis.disease_name}">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="DiseaseCrudServlet?action=Delete&disease_id=<c:out value="${dis.disease_id}"/>">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a>
        </td>
    </tr>
    </c:forEach>
    </tbody>
    </table>
        <ul class="pagination pagination-centered">
                        <li><a href="#">Prev</a></li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
    </div>
    </div>
    </div>

    </div>

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-apple"></i>Prescription</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Prescription ID</th>
                            <th>Disease Name</th>
                            <th>Short Prescription</th>
                            <th>Prescription</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                              <c:forEach items="${allPrescriptions}" var="pre">
                        <tr>
                            <td class="center">${pre.prescription_id}</td>
                            <td class="center">${pre.getDisease().getDisease_name()}</td>
                            <td class="center">${pre.short_desc}</td>
                            <td class="center">${pre.prescription}</td>
                            <td class="center">
                    <a data-toggle="model" class="btn btn-info pres-edit" data-prescriptionid="${pre.prescription_id}" data-diseaseid="${pre.getDisease().getDisease_id()}" data-diseasename="${pre.getDisease().getDisease_name()}" data-shortdesc="${pre.short_desc}" data-pres="${pre.prescription}">
                        <i class="glyphicon glyphicon-edit icon-white"></i>
                        Edit
                    </a>
                    <a class="btn btn-danger" href="PrescriptionCrudServlet?action=Delete&prescription_id=<c:out value="${pre.prescription_id}"/>">
                        <i class="glyphicon glyphicon-trash icon-white"></i>
                        Delete
                    </a>
                </td>
                        </tr>
                        </c:forEach>
                      
                        
                        </tbody>
                    </table>
                    <ul class="pagination pagination-centered">
                        <li><a href="#">Prev</a></li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-camera"></i>Image</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Prescription ID</th>
                            <th>Image ID</th>
                            <th>Image </th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${allImages}" var="pre">
                        <tr>
                            <td class="center">${pre.getPrescription().getPrescription_id()}</td>
                            <td class="center">${pre.image_id}</td>
                            <td class="center"><img src="uploaded-images/${pre.image_link}" alt="image" style="width: 100px; height: 100px;"></td>
                            
                            <td class="center">
                    <a data-toggle="model" class="btn btn-info image-edit" href="" data-prescriptionid="${pre.getPrescription().getPrescription_id()}" data-imageid="${pre.image_id}">
                        <i class="glyphicon glyphicon-edit icon-white"></i>
                        Edit
                    </a>
                    <a class="btn btn-danger" href="ImageCrudServlet?action=Delete&image_id=<c:out value="${pre.image_id}"/>">
                        <i class="glyphicon glyphicon-trash icon-white"></i>
                        Delete
                    </a>
                </td>
                        </tr>
                        </c:forEach>
                       
                        </tbody>
                    </table>
                    <ul class="pagination pagination-centered">
                        <li><a href="#">Prev</a></li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
    <footer>
        <div class="navbar navbar-inverse">
            <div class="container">
        <p class="copyright navbar-text pull-left">© 2016 Dr. Tababat</p>
        </div>
        </div>
    </footer>

</div>

<div class="modal fade" id="presedit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Editing</h3>
                </div>
                <div class="modal-body">
                    <form action="PrescriptionCrudServlet" method="post">
                          <input type="hidden" id="diseaseId" name="disease_id" value=""/>
                          <input type="hidden" id="presId" name="prescription_id" value=""/>
         <div class="form-group">
            <label class="control-label" for="diseaseName">Disease Name</label>
            <input type="text" id="diseaseName" class="form-control" name="disease_name" value=""/>
        </div>
        <div class="form-group">
            <label class="control-label" for="shortPres">Short Prescription(For Today's Advice)</label>
            <input type="text" id="shortPres" class="form-control" name="short_desc" value=""/>
        </div>
        <div class="form-group">
            <label class="control-label" for="Pres">Prescription</label>
            <input type="text" id="pres" class="form-control" name="prescription" value="" />
        </div>
       <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Save Changes"/>
         <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
        </form>
           </div>
                </div>
              
            </div>
        </div>
    </div>
        <div class="modal fade" id="diseaseedit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Editing</h3>
                </div>
                <div class="modal-body">
                    <form action="DiseaseCrudServlet" method="post" name="diseaseForm">
                     
         <div class="form-group dropdown">
   
             <select class="form-control" data-style="btn btn-success"  name="body_part"  id="body_part"  >
                 <optgroup label="Head" >
      <option >Hair</option>
      <option >Eyes</option>
      <option >Mouth</option>
      <option >Ear</option> 
                 </optgroup>
                 <optgroup label="Body">
    
      <option >Throat</option>
      <option >Chest</option>
      <option>Stomach</option>
                 </optgroup>
                 <optgroup label="Arm" >
      <option >Fingers</option>
      <option >Elbow</option>
                 </optgroup>
                 
                 <optgroup label="Leg">
      <option>Feet</option>             
      <option>Foot Fingers</option>
      <option>Knees</option>
                 </optgroup>
    </select>
         </div>
         
            <input type="hidden" id="disease_id" class="form-control"  name="disease_id" value=""/>
        
         <div class="form-group">
            <label class="control-label" for="diseaseName">Disease Name</label>
            <input type="text" id="disease_name" class="form-control"  name="disease_name" value="" />
        </div>
        <div class="modal-footer">
        <input class="btn btn-primary" type="submit" value="Save Changes"/>
         <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
        </form>
           </div>
           </div>
            </div>
        </div>
    </div>
        <div class="modal fade" id="imageedit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Editing</h3>
                </div>
                <div class="modal-body">
                    <form action="ImageCrudServlet" method="post">
                        <input type="hidden" id="imageId" name="image_id" value=""/>
        <div class="form-group">
            <label class="control-label" for="Pres">Prescription ID</label>
            <input class="form-control" id="presId" name="prescription_id" value="">
        </div>
        <div class="form-group">
           <i class="glyphicon glyphicon-camera"></i>
           <input id="image" type="file" name="image" >
          </div>
          <div class="modal-footer">
          <input class="btn btn-primary" type="submit" value="Save Changes"/>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
        </form>
          </div>
                </div>
              
            </div>
        </div>
    </div>
        
<div class="modal fade" id="setting" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                   
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

<script src="Admin.js"></script>


</body>
</html>
