<%-- 
    Document   : newjsp
    Created on : 04.03.2016, 12:26:35
    Author     : Natavan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Dr. Tababat</title>
    </head>
    <style>
        #logo{
            margin-top: -15px;
        } 
        jumbotrom{
            padding-top:70px;
            padding-bottom:100px;
        }
        .btn-group{
            width:100px;
        }
        .navbar-inverse{
            height:10px;
        }
        .navbar-inverse{           
            margin-bottom:0;
            margin-top:10%;
        }
    </style>
    
    <body>
        <div class = "navbar navbar-default navbar-top" role = "navigation">
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class="navbar-toggle" data-toggle = "collapse" data-target=".navbar-collapse">
                        <span class = "sr-only">Toggle Navigation</span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                    </button>
                    <a class = "navbar-brand"><img id = "logo" src = "log.png" width = 50 height = 50/></a>
                    <a class = "navbar-brand" href="main.jsp">Dr.Tababat</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">                  
                        <li class="active"><a href="main.jsp"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                        <li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> About</a></li>
                        <li><a href="#contact" data-toggle="modal"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="jumbotron text-center">
                <h1 style="color:green">Dr. Tababat</h1>
                <p>I'm capable of making healthy and positive decisions today!</p>
                <ul class="nav navbar-nav">
                    <li class = "dropdown">
                    <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Head<b class="caret"></b>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#">Hair</a></li>
                      <li><a class="dropdown-item" href="#">Eyes</a></li>
                      <li><a class="dropdown-item" href="#">Mouth</a></li>
                      <li><a class="dropdown-item" href="#">Ear</a></li>
                    </ul>
                  </div>
                    </li>
                    <li class = "dropdown">
                    <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Body<b class="caret"></b>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#">Throat</a></li>
                      <li><a class="dropdown-item" href="#">Chest</a></li>
                      <li><a class="dropdown-item" href="#">Stomach</a></li>
                    </ul>
                  </div>
                    </li>
                    <li class = "dropdown">
                    <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Arm<b class="caret"></b>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#">Fingers</a></li>
                      <li><a class="dropdown-item" href="#">Elbow</a></li>                     
                    </ul>
                  </div>
                    </li>
                    <li class = "dropdown">
                    <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Leg<b class="caret"></b>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#">Fingers</a></li>
                      <li><a class="dropdown-item" href="#">Knee</a></li>                     
                    </ul>
                  </div>
                    </li>                    
                </ul>
            </div>
            
            <div class="row">
             <div class="col-sm-8">
            <div class="row">
           <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Charisma Demo 2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px"
                 data-ad-client="ca-pub-5108790028230107"
                 data-ad-slot="3193373905"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>      

             </div>                 
             </div>
                pagination, adding new image to existing recipe, multiple presadd function. 
             <div class="col-sm-4">
               <div class="alert alert-info fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p><strong>Hey! Look at Today's Advice</strong></p>
                After eating banana, brush your teeth with its cover
              </div>
              
              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title">Search</h3>
                  </div>
                  <div class="panel-body text-center">
                      <form class="navbar-form navbar-left" role="search">
                <div class="form-group input-group">
                <input type="text" class="form-control" placeholder="Search..">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>        
                </div>
                </form>
                  </div>
                  
             </div>
            </div>
            </div>
        </div>
         
        <div class="navbar navbar-inverse navbar-bottom" role="navigation">
            <div class="container">
                <div class="navbar-text pull-left">
                   <p>© 2016 Dr. Tababat</p>
                </div>
            </div>
            </div>
        
        <!-- Contact-->
          <div class="modal fade" id="contact" roles="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" role="form">
                        <div class="modal-header">
                            <h4>Contact</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="contact-name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact-name" placeholder="First & Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="contact-email" placeholder="example@domain.com">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-message" class="col-sm-2 control-label">Message</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                            
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </body>
</html>
