<%-- 
    Document   : newjsp
    Created on : Mar 27, 2016, 1:07:21 PM
    Author     : Sariya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <style>
            .tableRow {
        display: table-row;
    }
    .tableCell {
        display: table-cell;
    }
      </style>
            <div class="box-content col-md-6">
            <div class="form-group">
           <div class="table">
            <div class="tableRow">
                <div class="tableCell">
                       <i class="glyphicon glyphicon-camera"></i>
                    <input type="file" id="file">
                </div>
                <div class="tableCell" id="fileInfo">
            </div>
            </div>
           <div class="tableCell box1">
                    <h2>Image</h2>
                    <img src="" id="uploadedImage" style="width: 170px; height: 100px; background-color:#efeeee; border: 0px; ">
                   <br>
                    <button id="uploadImage" class="btn btn-primary">Upload</button>
                </div>
           
        </div>
            </div>
                

                
          </div> 
      <script src="uploadfile.js"></script>
    </body>
</html>
