package com.tababat.controller;

import com.tababat.db.DiseaseDb;
import com.tababat.db.ImageDb;
import com.tababat.db.PrescriptionDb;
import com.tababat.entities.Disease;
import com.tababat.entities.Image;
import com.tababat.entities.Prescription;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sariya
 */
public class AddPrescriptionServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            if (request.getParameter("disease_name") == null || request.getParameter("prescription") == null || (request.getParameter("disease_name").equals("") || request.getParameter("prescription").equals(""))) {
                System.out.println(request.getParameter("disease_name"));
                out.println("<script type=\"text/javascript\">");   //alert when disease name or prescription is null
                out.println("alert('Disease name or Prescription cannot be null');");
                out.println("location='AdminPnl.jsp';");
                out.println("</script>");

            } else {
                HttpSession session = request.getSession(false); //session for getting image name
                ArrayList links = (ArrayList) session.getAttribute("links");

                String disease_name = request.getParameter("disease_name");
                String body_part = request.getParameter("body_part");

                String prescription = request.getParameter("prescription");

                Disease disease = DiseaseDb.getDiseaseByName(disease_name);//check if disease exists
                if (disease == null) { //if does not exist create new disease
                    disease = new Disease();
                    disease.setDisease_name(disease_name);
                    disease.setBody_part(body_part);
                    DiseaseDb.insert(disease);
                }
                Prescription pres = new Prescription();
                pres.setPrescription(prescription);
                pres.setDisease(disease); //set disease to a new prescription
                if ((request.getParameter("short_desc") != null) || (!request.getParameter("short_desc").equals(""))) {
                    String short_desc = request.getParameter("short_desc");
                    pres.setShort_desc(short_desc);
                }
                PrescriptionDb.insert(pres);

                if (links != null) { //iterate thorugh arraylist and set image links to prescription
                    Iterator iterator = links.iterator();
                    while (iterator.hasNext()) {
                        String link = (String) iterator.next();
                        Image image = new Image();
                        image.setImage_link(link);
                        image.setPrescription(pres);
                        ImageDb.insert(image);
                    }
                    links.clear();
                } else {
                    System.out.println("No image to set");
                }

                response.sendRedirect("AdminPnl_Table.jsp");

            }
        } catch (Exception e) {
            System.out.println("Exception occured");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
