/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.controller;

import com.tababat.db.DiseaseDb;
import com.tababat.entities.Disease;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class DiseaseCrudServlet extends HttpServlet {

    private static String Crud = "AdminPnl_Table.jsp";
    private static String Update = "";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");
            String forward = "";
            String disease_idstr = request.getParameter("disease_id");
            int disease_id = 0;
            try {
                if (disease_idstr != null || !disease_idstr.equals("")) {
                    disease_id = Integer.parseInt(disease_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {

                DiseaseDb.delete(disease_id);
                forward = Crud;
            } else if (action.equals("Update")) {
                Disease disease = DiseaseDb.getDiseaseById(disease_id);
                request.setAttribute("dis", disease);
                forward = Crud;
            } else {
                forward = Crud;
            }
            request.getRequestDispatcher(forward).forward(request, response);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String forward = "";
            String disease_idstr = request.getParameter("disease_id");
            int disease_id = 0;
            try {
                if (disease_idstr != null || !disease_idstr.equals("")) {
                    disease_id = Integer.parseInt(disease_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            Disease disease = DiseaseDb.getDiseaseById(disease_id);
            String disease_name = request.getParameter("disease_name");
            String body_part = request.getParameter("body_part");

            if (!disease_name.equals("") && !body_part.equals("")) {
                disease.setDisease_name(disease_name);
                disease.setBody_part(body_part);
                DiseaseDb.update(disease);
                forward = Crud;

            } else {
                forward = Crud;
            }

            request.getRequestDispatcher(forward).forward(request, response);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
