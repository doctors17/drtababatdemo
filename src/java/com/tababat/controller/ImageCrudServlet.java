/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.controller;

import com.tababat.db.ImageDb;
import com.tababat.db.PrescriptionDb;
import com.tababat.entities.Image;
import com.tababat.entities.Prescription;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class ImageCrudServlet extends HttpServlet {

    private static String Crud = "AdminPnl_Table.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String action = request.getParameter("action");
            String image_idstr = request.getParameter("image_id");
            String forward = "";
            int image_id = 0;
            try {
                if (image_idstr != null || !image_idstr.equals("")) {

                    image_id = Integer.parseInt(image_idstr);
                }
            } catch (NumberFormatException e) {

            }
            if (action.equals("Delete")) {
                ImageDb.delete(image_id);
                System.out.println("Deleted image");
                forward = Crud;
            } else {
                forward = Crud;
            }
            request.getRequestDispatcher(forward).forward(request, response);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String image_idstr = request.getParameter("image_id");
            String forward = "";
            int image_id = 0;
            try {
                if (image_idstr != null || !image_idstr.equals("")) {

                    image_id = Integer.parseInt(image_idstr);
                }
            } catch (NumberFormatException e) {

            }
            String prescription_idstr = request.getParameter("prescription_id");
            int prescription_id = 0;
            try {
                if (prescription_idstr != null || !prescription_idstr.equals("")) {

                    prescription_id = Integer.parseInt(prescription_idstr);
                }
            } catch (NumberFormatException e) {

            }
            String image_link = request.getParameter("image_link");

            Image image = ImageDb.getImageById(image_id);
            Prescription pres = PrescriptionDb.getPrescriptionById(prescription_id);

            try {
                if (image_link == null || image_link.equals("")) {

                } else {
                    image.setImage_link(image_link);
                    image.setPrescription(pres);
                    ImageDb.update(image);
                    forward = Crud;
                }
            } catch (NullPointerException e) {
                System.out.println("null!!");
            }

            request.getRequestDispatcher(forward).forward(request, response);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
