package com.tababat.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 5, // 5 MB
        maxRequestSize = 1024 * 1024 * 10)
// 10 MB
public class UploadFile extends HttpServlet {

    volatile ArrayList links;
    String saveFile = "C:\\Users\\Sariya\\Desktop\\newFolder\\DrTababat\\web\\uploaded-images";
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            String fileName = null;

            //Writing image or file
            Part part = request.getPart("file");
            fileName = getFileName(part);
            links.add(fileName);
            System.out.println(links);
            session.setAttribute("links", links);
            part.write(saveFile + File.separator + fileName);

            // Extra logic to support multiple domain 
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.getWriter().print(fileName + " uploaded successfully");
        } catch (Exception e) {
            System.out.println("Exception!!!!!!");
            response.getWriter().print(" Nothing to upload");
        }
    }

    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= " + contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2,
                        token.length() - 1);
            }
        }
        return "";
    }

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        links = new ArrayList();
    }

}
