/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.db;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Sariya
 */
public class DbUtil {
    private static final EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("DrTababatPU");
  public static EntityManagerFactory getEmfactory(){
      return emfactory;
  }
}
