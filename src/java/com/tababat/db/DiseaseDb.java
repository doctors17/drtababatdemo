/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.db;

import com.tababat.entities.Disease;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class DiseaseDb {

    public static Disease getDiseaseById(int disease_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Disease disease = em.find(Disease.class, disease_id);
            return disease;
        } finally {
            em.close();
        }

    }

    public static Disease getDiseaseByName(String disease_name) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Disease d where d.disease_name=:disease_name";
        TypedQuery<Disease> q = em.createQuery(query, Disease.class);
        q.setParameter("disease_name", disease_name);
        Disease disease = null;
        try {
            disease = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println(e);

        } finally {
            em.close();
        }
        return disease;
    }

    public static List<Disease> getDiseaseByBodyPart(String body_part) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Disease d where d.body_part=:body_part";
        TypedQuery<Disease> q = em.createQuery(query, Disease.class);
        q.setParameter("body_part", body_part);
        List<Disease> diseases;
        try {
            diseases = q.getResultList();
            if (diseases == null || diseases.isEmpty()) {
                diseases = null;
            }

        } finally {
            em.close();
        }
        return diseases;
    }

    public static List<Disease> getAllDiseases() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Disease.getAll").getResultList();
        } finally {

            em.close();
        }
    }

    public static void insert(Disease disease) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(disease);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }

    public static void update(Disease disease) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(disease);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }

    public static void delete(int disease_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Disease disease = em.find(Disease.class, disease_id);
            em.remove(disease);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
