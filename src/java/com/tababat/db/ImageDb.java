/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.db;

import com.tababat.entities.Image;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class ImageDb {
      public static Image getImageById(int image_id){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
        try{
        Image image=em.find(Image.class, image_id);
            return image;
        }finally{
            em.close();
        }
        
    }
       public static List<Image> getImageByDPrescriptionId(int prescription_id){
           EntityManager em = DbUtil.getEmfactory().createEntityManager();
             String query = "select i from Image i where i.prescription_id=:prescription_id";
        TypedQuery<Image> q = em.createQuery(query, Image.class);
        q.setParameter("prescription_id", prescription_id);
        List<Image> images;
        try {
            images = q.getResultList();
            if ( images == null ||  images.isEmpty()) {
                images = null;
            }

        } finally {
            em.close();
        }
        return  images;
     }
        public static List<Image> getAllImages() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Image.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    public static void insert(Image image){
         EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.persist(image);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
    public static void update(Image image){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.merge(image);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
    public static void delete(int image_id){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             Image image=em.find(Image.class, image_id);
             em.remove(image);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
}
