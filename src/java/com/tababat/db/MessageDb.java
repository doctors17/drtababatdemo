/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.db;

import com.tababat.entities.Message;
import com.tababat.entities.Prescription;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class MessageDb {
      public static Message getMessageById(int message_id){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
        try{
         Message message=em.find(Message.class, message_id);
            return message;
        }finally{
            em.close();
        }
        
    }
       public static Prescription getMessageByName(String message_name){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
        try{
            
         Prescription prescription=em.find(Prescription.class, message_name);
            return prescription;
        }finally{
            em.close();
        }
        
    }
     public static List<Message> getAllMessages() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Message.getAll").getResultList();
        } finally {

            em.close();
        }
    }
     public static List<Message> getMessageByDiseaseId(int message_id){
           EntityManager em = DbUtil.getEmfactory().createEntityManager();
             String query = "select p from Message p where p.message_id=:message_id";
        TypedQuery<Message> q = em.createQuery(query, Message.class);
        q.setParameter("message_id", message_id);
        List<Message> messages;
        try {
           messages = q.getResultList();
            if ( messages == null ||  messages.isEmpty()) {
                messages = null;
            }

        } finally {
            em.close();
        }
        return  messages;
     }
    
    public static void insert(Message message){
         EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.persist(message);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
    public static void update(Message message){
         EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.merge(message);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
             
         }finally{
             em.close();
         }
    }
    public static void delete(int message_id){
         EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             Message message=em.find(Message.class, message_id);
             em.remove(message);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
}
