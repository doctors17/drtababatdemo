/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.db;

import com.tababat.entities.Prescription;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class PrescriptionDb {
     public static Prescription getPrescriptionById(int prescription_id){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
        try{
         Prescription prescription=em.find(Prescription.class, prescription_id);
            return prescription;
        }finally{
            em.close();
        }
        
    }
       public static Prescription getPrescriptionByName(String prescription_name){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
        try{
            
         Prescription prescription=em.find(Prescription.class, prescription_name);
            return prescription;
        }finally{
            em.close();
        }
        
    }
     public static List<Prescription> getAllPrescriptions() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Prescription.getAll").getResultList();
        } finally {

            em.close();
        }
    }
     public static List<Prescription> getPrescriptionByDiseaseId(int disease_id){
           EntityManager em = DbUtil.getEmfactory().createEntityManager();
             String query = "select p from Prescription p where p.disease_id=:disease_id";
        TypedQuery<Prescription> q = em.createQuery(query, Prescription.class);
        q.setParameter("disease_id", disease_id);
        List<Prescription> prescriptions;
        try {
            prescriptions = q.getResultList();
            if ( prescriptions == null ||  prescriptions.isEmpty()) {
                prescriptions = null;
            }

        } finally {
            em.close();
        }
        return  prescriptions;
     }
    
    public static void insert(Prescription prescription){
         EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.persist(prescription);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
    public static void update(Prescription prescription){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             em.merge(prescription);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
             
         }finally{
             em.close();
         }
    }
    public static void delete(int prescription_id){
        EntityManager em=DbUtil.getEmfactory().createEntityManager();
         EntityTransaction trans=em.getTransaction();
         trans.begin();
         try{
             Prescription prescription=em.find(Prescription.class, prescription_id);
             em.remove(prescription);
             trans.commit();
         }catch(Exception e){
             System.out.println(e);
             trans.rollback();
         }finally{
             em.close();
         }
    }
}
