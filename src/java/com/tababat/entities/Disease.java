/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.entities;
import com.tababat.filter.Filter;
import java.io.Serializable;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
@NamedQuery(name="Disease.getAll", query="select d from Disease d"),
@NamedQuery(name="Disease.getByName", query="select d from Disease d where d.disease_name=:disease_name"),
@NamedQuery(name="Disease.getByBodyPart", query="select d from Disease d where d.body_part=:body_part")})
public class Disease implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int disease_id;
    @Column(nullable = false, unique=true, length = 100)
    private String disease_name;
    @Column(nullable = false, length = 200)
    private String body_part;
    @OneToMany(mappedBy = "disease_id", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private List <Prescription> prescriptions;
    
    public Disease(){
        prescriptions=new ArrayList<Prescription> ();
    }
    public int getDisease_id() {
        return disease_id;
    }

    public void setDisease_id(int disease_id) {
        this.disease_id = disease_id;
    }

    public String getDisease_name() {
        return disease_name;
    }

    public void setDisease_name(String disease_name) {
        this.disease_name = Filter.filter(disease_name);
    }

    public String getBody_part() {
        return body_part;
    }

    public void setBody_part(String body_part) {
        this.body_part = Filter.filter(body_part);
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        
        this.prescriptions = prescriptions;
    }
    
    
}
