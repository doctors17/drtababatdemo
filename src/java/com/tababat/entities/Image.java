/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.entities;
import com.tababat.filter.Filter;
import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQuery(name="Image.getAll", query="select p from Image p")
public class Image implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int image_id;
    @Column(nullable = false, length = 300)
    private String image_link;
    @ManyToOne(targetEntity = Prescription.class)
    @JoinColumn(name="prescription_id", referencedColumnName = "prescription_id")
    private Prescription prescription_id;
  public Image(){
      
  }
    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = Filter.filter(image_link);
    }

    public Prescription getPrescription() {
        return prescription_id;
    }

    public void setPrescription(Prescription prescription_id) {
        this.prescription_id = prescription_id;
    }
    
    
}
