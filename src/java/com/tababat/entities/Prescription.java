/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tababat.entities;
import com.tababat.filter.Filter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQuery(name="Prescription.getAll", query="select p from Prescription p")
public class Prescription implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int prescription_id;
    @Column(nullable = false, length=1000)
    private String prescription;
    @Column(nullable = true, length=300)
    private String short_desc;
    @ManyToOne(targetEntity = Disease.class, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name="disease_id", referencedColumnName = "disease_id")
    private Disease disease_id;
    @OneToMany(mappedBy = "prescription_id", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private List<Image> images;
    
    public Prescription(){
        images=new ArrayList<Image>();
    }
    public int getPrescription_id() {
        return prescription_id;
    }

    public void setPrescription_id(int prescription_id) {
        this.prescription_id = prescription_id;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = Filter.filter(prescription);
    }

    public String getShort_desc() {
        return short_desc;
    }

    public void setShort_desc(String short_desc) {
        this.short_desc = Filter.filter(short_desc);
    }

    public Disease getDisease() {
        return disease_id;
    }

    public void setDisease(Disease disease_id) {
        this.disease_id = disease_id;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
    
    
}
