
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * author: Natavan
 */
$(document).ready(function () {
    $('.btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.btn-minimize').click(function (e) {
        e.preventDefault();
        s
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible'))
            $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else
            $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $target.slideToggle();
    });
    $('.btn-setting').click(function (e) {
        e.preventDefault();
        $('#setting').modal('show');
    });
    $('.disease-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #body_part').val($(this).data('id2'));
        $('.modal-body #disease_id').val($(this).data('id'));
        $('.modal-body #disease_name').val($(this).data('id3'));
        $('#diseaseedit').modal('show');


    });
    $('.pres-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #presId').val($(this).data('prescriptionid'));
        $('.modal-body #diseaseId').val($(this).data('diseaseid'));
        $('.modal-body #diseaseName').val($(this).data('diseasename'));
        $('.modal-body #shortPres').val($(this).data('shortdesc'));
        $('.modal-body #pres').val($(this).data('pres'));

        $('#presedit').modal('show');
    });
    $('.image-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #imageId').val($(this).data('imageid'));
        $('.modal-body #presId').val($(this).data('prescriptionid'));
        $('#imageedit').modal('show');
    });
// document.getElementById("image").onchange = function () {
//    var reader = new FileReader();
//
//    reader.onload = function (e) {
//        // get loaded data and render thumbnail.
//        document.getElementById("myImg").src = e.target.result;
//    };
//
//    // read the image file as a data URL.
//    reader.readAsDataURL(this.files[0]);
//};
//show uploaded images
  

});
  var counter = 1;
    var limit = 30;
    function addInputForPrescription(divName1) {

        if (counter == limit) {
            alert("You have reached the limit of adding " + counter + " prescriptions");
        }
        else {
            var newdiv1 = document.createElement('div');
            newdiv1.innerHTML = "</br> <textarea id='Pres' class='form-control' rows='4' name='prescription'></textarea>";
            document.getElementById(divName1).appendChild(newdiv1);
            counter++;
        }
    }