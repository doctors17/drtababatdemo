<%-- 
    Document   : Admin
    Created on : 07.03.2016, 14:45:30
    Author     :Natavan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Admin Panel</title>
    </head>

    <body>

        <style>
            #logo{
                margin-top: -15px;
            }
            a:hover{
                text-decoration: none;
            }
            .search-query{
                margin-right: 30px;
            }
            ul{
                list-style: none;
            }
            ul li{
                float: left;
                margin:10px 10px 0px;
            }
            .navbar-inverse{           
                margin-bottom:0;
                margin-top:22%;
            }
            .dropdown-header{
                width:100%;
            }
            .table {
                display: table;
                border-collapse: separate;
                border-spacing: 40px 50px;
            }
            .tableRow {
                display: table-row;
            }
            .tableCell {
                display: table-cell;
            }
            .box1 {
                border: 10px solid rgba(238, 238, 238, 0.5);
                width: 250px;
                height: 200px;
            }
        </style>
        <div class="navbar navbar-default" role="navigation">

            <div class="navbar-inner">
                <button type="button" class="navbar-toggle pull-right animated flip" data-toggle = "collapse" data-target=".main-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class = "navbar-brand"><img id = "logo" src = "log.png" width = "50" height = "50"/></a>
                <a class = "navbar-brand" href="main.jsp">Dr.Tababat</a>

                <div class="navbar-collapse nav navbar-nav top-menu btn-group pull-right">
                    <ul>
                        <li>
                            <form class="navbar-search">                        
                                <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                                       type="text">
                            </form>
                        </li>
                        <li><a href="#">
                                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                <span class="label label-info">3</span>
                            </a>
                        </li>
                        <li><a href="main.jsp"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li> 
                        <li><a href="AdminReg.jsp">
                                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                                log out
                            </a>
                        </li>                
                    </ul> 
                </div>            
            </div>
        </div>

        <div class="ch-container">
            <div class="row">

                <!-- left menu -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">

                            </div>
                            <ul class="collapse navbar-collapse nav nav-pills nav-stacked main-menu">
                                <li class="nav-header">Navigation</li>
                                <li class="active"><a class="ajax-link" href="AdminPnl.jsp"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                                </li>
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
                                </li>
                                <li><a class="ajax-link" href="AdminPnl_Table.jsp"><i
                                            class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>                        
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                                </li>
                            </ul>                    
                        </div>
                    </div>
                </div>

                <noscript>
                <div class="alert alert-block col-md-12">
                    <h4 class="alert-heading">Warning!</h4>

                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                        enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="col-lg-10 col-sm-10">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i> Add Prescription</h2>        
                                </div>
                                <form action="AddPrescriptionServlet" method="post" >
                                    <div class="box-content col-md-6">
                                        <div class="form-group dropdown">

                                            <select class="form-control" data-style="btn btn-success"  name="body_part"  id="body_part"  >
                                                <optgroup label="Head" >
                                                    <option >Hair</option>
                                                    <option >Head</option>
                                                    <option >Eyes</option>
                                                    <option >Mouth</option>
                                                    <option >Ear</option> 
                                                </optgroup>
                                                <optgroup label="Body">

                                                    <option >Throat</option>
                                                    <option >Chest</option>
                                                    <option>Stomach</option>
                                                </optgroup>
                                                <optgroup label="Arm" >
                                                    <option >Fingers</option>
                                                    <option >Elbow</option>
                                                </optgroup>

                                                <optgroup label="Leg">
                                                    <option>Feet</option>             
                                                    <option>Foot Fingers</option>
                                                    <option>Knees</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="diseaseName">Disease Name</label>
                                            <input type="text" id="diseaseName" class="form-control" name="disease_name"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="shortPres">Short Prescription(For Today's Advice)</label>
                                            <input type="text" id="shortPres" class="form-control" name="short_desc"/>
                                        </div>
                                        <div class="form-group" id="prescript">
                                            <label class="control-label" for="Pres">Prescription</label>
                                            <textarea id="Pres" class="form-control" rows="4" name="prescription"></textarea>

                                        </div>                                                 
                                        <i class="glyphicon glyphicon-plus-sign" style="font-size: 30px" onclick="addInputForPrescription('prescript')"></i>                         

                                     </br>

                                        <button type="submit" class="btn btn-success">Add</button>  
                                    </div>
                                </form>
                            </div>


                            <div class="box-content col-md-6">
                                <div class="form-group">
                                    <div class="table">
                                        <div class="tableRow">
                                            <div class="tableCell">
                                                <i class="glyphicon glyphicon-camera"></i>
                                                <input type="file" id="file">
                                            </div>
                                            <div class="tableCell" id="fileInfo">
                                            </div>
                                        </div>
                                        <div class="tableCell box1">
                                            <h2>Image</h2>
                                            <img src="" id="uploadedImage" style="width: 170px; height: 100px; background-color:#efeeee; border: 0px; ">
                                            <br>
                                            <button id="uploadImage" class="btn btn-primary">Upload</button>
                                        </div>

                                    </div>
                                </div>



                            </div>       


                        </div>
                    </div>

                </div>
            </div>
            <footer>
                <div class="navbar navbar-inverse">
                    <div class="container">
                        <p class="copyright navbar-text pull-left">© 2016 Dr. Tababat</p>
                    </div>
                </div>
            </footer>

        </div>
        <script src="uploadfile.js"></script>
        <script src="Admin.js"></script>
    </body>
</html>
