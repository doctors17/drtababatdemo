<%-- 
    Document   : Crud
    Created on : Mar 8, 2016, 12:44:37 AM
    Author     : Sariya
--%>
<%@page import="com.tababat.db.ImageDb"%>
<%@page import="com.tababat.db.PrescriptionDb"%>
<%@page import="com.tababat.db.DiseaseDb"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Panel</title>
    </head>
    <body>
                <% 
                 request.setAttribute("allDiseases", DiseaseDb.getAllDiseases());
                 request.setAttribute("allPrescriptions", PrescriptionDb.getAllPrescriptions());
                 request.setAttribute("allImages", ImageDb.getAllImages());
                %>
                <form action="DiseaseCrudServlet" method="post">
         <table border="1">
             <tr>
            <th>Disease ID</th>
            <th>Body Part</th>
            <th>Disease Name</th>
            <th>Actions</th>
            </tr>
           
            <c:forEach items="${allDiseases}" var="dis">
                <tr>
                    <td>${dis.disease_id}</td>
                    <td>${dis.body_part}</td>
                    <td>${dis.disease_name}</td>
                    <td><a href="DiseaseCrudServlet?action=Update&disease_id=<c:out value="${dis.disease_id}"/>">Update</a></td>
                    <td><a href="DiseaseCrudServlet?action=Delete&disease_id=<c:out value="${dis.disease_id}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </table> 
                    </form>
         <br/>
         <br/>
         <form action="PrescriptionCrudServlet" method="post">
          <table border="1">
             <tr>
            <th>Prescription ID</th>
            <th>Short Description</th>
            <th>Prescription</th>
            <th>Disease ID</th>
             <th>Disease Name</th>
            </tr>
           
            <c:forEach items="${allPrescriptions}" var="pre">
                <tr>
                    <td>${pre.prescription_id}</td>
                    <td>${pre.short_desc}</td>
                    <td>${pre.prescription}</td>
                    <td>${pre.getDisease().getDisease_id()}</td>
                    <td>${pre.getDisease().getDisease_name()}</td>
                    <td><a href="PrescriptionCrudServlet?action=Update&prescription_id=<c:out value="${pre.prescription_id}"/>">Update</a></td>
                    <td><a href="PrescriptionCrudServlet?action=Delete&prescription_id=<c:out value="${pre.prescription_id}"/>">Delete</a></td>
                </tr>
            </c:forEach>
                
        </table> 
             </form>
         <br/>
         <br/>
         <form action="ImageCrudServlet" method="post">
                   <table border="1">
             <tr>
            <th>Image ID</th>
            <th>Image Link</th>
            <th>Prescription ID</th>
            
            </tr>
           
            <c:forEach items="${allImages}" var="pre">
                <tr>
                    <td>${pre.image_id} </td>
                    <td> ${pre.image_link}</td>
                    <td>${pre.getPrescription().getPrescription_id()}</td>
                    <td><a href="ImageCrudServlet?action=Update&image_id=<c:out value="${pre.image_id}"/>">Update</a></td>
                    <td><a href="ImageCrudServlet?action=Delete&image_id=<c:out value="${pre.image_id}"/>">Delete</a></td>
                </tr>
            </c:forEach>
                
        </table> 
             </form>
         <form method="post" action="AddFile" enctype="multipart/form-data">
             Choose File:<input id="image" type="file" name="item"/>
       
               <img id="myImg" src="#" alt="your image" />
                </form>
    </body>
</html>
